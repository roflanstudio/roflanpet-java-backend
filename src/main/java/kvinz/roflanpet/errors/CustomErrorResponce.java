package kvinz.roflanpet.errors;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class CustomErrorResponce {
    private String error;
}
