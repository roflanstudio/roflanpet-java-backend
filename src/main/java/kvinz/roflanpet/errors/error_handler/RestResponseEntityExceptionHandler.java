package kvinz.roflanpet.errors.error_handler;


import kvinz.roflanpet.errors.BadRequestException;
import kvinz.roflanpet.errors.CustomErrorResponce;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler{

    @ExceptionHandler({BadRequestException.class})
    public ResponseEntity<CustomErrorResponce> handleBadRequestException(BadRequestException exception){
        return new ResponseEntity<>(new CustomErrorResponce(exception.getMessage()),HttpStatus.BAD_REQUEST);
    }
}
