package kvinz.roflanpet.service;

import kvinz.roflanpet.domain.Species;

import java.util.List;

public interface SpeciesService {
    List<Species> findAllSpecies();
}
