package kvinz.roflanpet.service;

import kvinz.roflanpet.domain.Leaflet;
import kvinz.roflanpet.domain.Pet;
import kvinz.roflanpet.model.MatchingModel;
import kvinz.roflanpet.repositorie.LeafletRepository;
import kvinz.roflanpet.repositorie.PetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class PetServiceImpl implements PetService {


    @Autowired
    LeafletRepository leafletRepository;

    @Autowired
    EmailService emailService;

    @Override
    public MatchingModel findMatching(Integer leafletId) {
        List<Leaflet> regularmatchingLeaflets;
        List<Leaflet> strongGenderMatchingLeaflets = new ArrayList<>();
        List<Leaflet> strongSizeMatchingLeaflets = new ArrayList<>();
        List<Leaflet> veryStrongMatching;
        Leaflet leaflet = leafletRepository.findById(leafletId).orElseGet( () -> null);
        if(leaflet == null) return new MatchingModel();
        regularmatchingLeaflets = leafletRepository.findAllByStatusIsNot(leaflet.getStatus());
        regularmatchingLeaflets = regularmatchingLeaflets.stream()
                .filter(leaflet1 -> leaflet1.getPet().getSpecies().getName()
                        .equals(leaflet.getPet().getSpecies().getName()))
                .filter(leaflet1 -> leaflet1.getCity().equals(leaflet.getCity()))
                .collect(Collectors.toList());
        regularmatchingLeaflets
                .forEach(leaflet1 -> {
                    if(leaflet1.getPet().getGender()!= null && leaflet1.getPet().getGender().equals(leaflet.getPet().getGender())){
                        strongGenderMatchingLeaflets.add(leaflet1);
                    }
                    if(leaflet1.getPet().getSize()!= null && leaflet1.getPet().getSize().equals(leaflet.getPet().getSize())){
                        strongSizeMatchingLeaflets.add(leaflet1);
                    }
                });
        veryStrongMatching  = new ArrayList<>(strongGenderMatchingLeaflets);
        veryStrongMatching.retainAll(strongSizeMatchingLeaflets);
        MatchingModel matchingModel = new MatchingModel(regularmatchingLeaflets,strongGenderMatchingLeaflets
                ,strongSizeMatchingLeaflets,veryStrongMatching);

        if(!regularmatchingLeaflets.isEmpty()){
            emailService
                    .sendMatchingEmail(leaflet.getCreator().getEmail(),matchingModel);
        }
        return matchingModel;
    }
}
