package kvinz.roflanpet.service;

import kvinz.roflanpet.domain.Breed;
import kvinz.roflanpet.repositorie.BreedRepository;
import kvinz.roflanpet.repositorie.SpeciesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BreedServiceImpl implements BreedService {
    @Autowired
    BreedRepository breedRepository;

    @Override
    public List<Breed> findBreedBySpeciesId(Integer id) {
        return breedRepository.findBySpeciesId_Id(id);
    }
}
