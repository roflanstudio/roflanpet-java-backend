package kvinz.roflanpet.service;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;
import kvinz.roflanpet.domain.Leaflet;
import kvinz.roflanpet.repositorie.LeafletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class LeafletServiceImpl implements LeafletService {



    @Autowired
    private LeafletRepository leafletRepository;

    public List<Leaflet> findAllLeaflets(){
        return leafletRepository.findAll();
    }

    @Override
    public Leaflet fondLeafletsById(Integer id) {
        return leafletRepository.findById(id).orElse(new Leaflet());
    }

    public List<Leaflet> findLeafletsAndSortByValueWithFilter(Integer pageNo, Integer pageSize,
                                                              String sortBy,String status,Integer cityId,
                                                              String petGender, String petSize,Integer petSpeciesId,
                                                              Integer petBreadId){
        Pageable paging;
        List<Leaflet> leaflets = new ArrayList<>();
        if(sortBy.startsWith("-")){
            sortBy = sortBy.replace("-","");
            paging = PageRequest.of(pageNo, pageSize, Sort.by(Sort.Direction.DESC, sortBy));
        }else{
            paging = PageRequest.of(pageNo, pageSize, Sort.by(Sort.Direction.ASC, sortBy));
        }
        if(status != null){
            leaflets = leafletRepository.findAllByResolvedFalseAndStatus(paging,status);
        }else{
            leaflets = leafletRepository.findAllByResolvedFalse(paging);
        }
        if(cityId != null){
            leaflets = leaflets.stream()
                    .filter(i->i.getCity()!=null)
                    .filter(i->i.getCity().getId().equals(cityId))
                    .collect(Collectors.toList());
        }
        if(!petGender.isEmpty()){
            leaflets = leaflets.stream()
                    .filter(i->
                           i.getPet().getGender().equals(petGender)
                    )
                    .collect(Collectors.toList());
        }
        if(!petSize.isEmpty()){
            leaflets = leaflets.stream()
                    .filter(i-> i.getPet().getSize().equals(petSize))
                    .collect(Collectors.toList());
        }
        if(petSpeciesId!=null){
            leaflets = leaflets.stream()
                    .filter(i->i.getPet()!=null)
                    .filter(i->i.getPet().getSpecies()!=null)
                    .filter(i->i.getPet().getSpecies().getId().equals(petSpeciesId))
                    .collect(Collectors.toList());
        }

        if(petBreadId!=null){
            leaflets = leaflets.stream()
                    .filter(i->i.getPet().getBreed()!=null)
                    .filter(i->i.getPet().getBreed().getId().equals(petBreadId)).collect(Collectors.toList());
        }



        return  leaflets;
    }
}
