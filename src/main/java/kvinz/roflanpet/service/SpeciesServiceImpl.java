package kvinz.roflanpet.service;

import kvinz.roflanpet.domain.Species;
import kvinz.roflanpet.repositorie.SpeciesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SpeciesServiceImpl implements SpeciesService {

    @Autowired
    SpeciesRepository speciesRepository;

    @Override
    public List<Species> findAllSpecies() {
        return speciesRepository.findAll();
    }
}
