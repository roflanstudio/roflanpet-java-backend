package kvinz.roflanpet.service;

import kvinz.roflanpet.domain.Breed;

import java.util.List;

public interface BreedService {
    List<Breed> findBreedBySpeciesId(Integer id);
}
