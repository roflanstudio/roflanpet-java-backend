package kvinz.roflanpet.service;

import kvinz.roflanpet.domain.Leaflet;
import kvinz.roflanpet.model.MatchingModel;

import java.util.List;

public interface PetService {
    MatchingModel findMatching(Integer leafletId);
}
