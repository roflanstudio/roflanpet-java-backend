package kvinz.roflanpet.service;

import kvinz.roflanpet.domain.City;
import kvinz.roflanpet.repositorie.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceImpl implements CityService {

    @Autowired
    CityRepository cityRepository;

    @Override
    public List<City> findAllCity() {
        return cityRepository.findAll();
    }
}
