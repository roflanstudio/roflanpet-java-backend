package kvinz.roflanpet.service;

import kvinz.roflanpet.model.MatchingModel;
import org.springframework.stereotype.Service;


public interface EmailService {
    void sendMatchingEmail(String email, MatchingModel matchingModel);
}
