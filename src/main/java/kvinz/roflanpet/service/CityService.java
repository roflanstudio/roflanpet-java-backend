package kvinz.roflanpet.service;

import kvinz.roflanpet.domain.City;

import java.util.List;

public interface CityService {
    List<City> findAllCity();
}
