package kvinz.roflanpet.service;

import kvinz.roflanpet.domain.Leaflet;
import kvinz.roflanpet.model.MatchingModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class EmailServiceImpl implements EmailService {
    @Autowired
    private JavaMailSender emailSender;

    private void formEmail(String toAddress,String fromAddress,String subject,String content){
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        try {
            helper.setFrom(fromAddress);
            helper.setTo(toAddress);
            helper.setSubject(subject);
            helper.setText(content, true);
            emailSender.send(message);
        }catch (Exception e){  }
    }

    @Override
    public void sendMatchingEmail(String email, MatchingModel matchingModel) {
        List<Leaflet> leaflets = Stream.of(matchingModel.getRegularMatchingLeaflets(),
                matchingModel.getStrongGenderMatchingLeaflets(),
                matchingModel.getStrongSizeMatchingLeaflets(),matchingModel.getVeryStrongMatching())
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
        leaflets = leaflets.stream()
                .distinct()
                .collect(Collectors.toList());
        StringBuilder Urls = new StringBuilder();
        leaflets.forEach(leaflet -> {
            Urls.append("https://roflanpet-frontend.herokuapp.com/main/catalog/").append(leaflet.getId()).append("<br>");
        });
        if(!matchingModel.getVeryStrongMatching().isEmpty()){
            Urls.append("Из них максимально подходят под описание:<br>");
            matchingModel.getVeryStrongMatching().forEach(leaflet -> {
                 Urls.append("https://roflanpet-frontend.herokuapp.com/main/catalog/")
                        .append(leaflet.getId()).append("<br>");
            });
        }
        String fromAddress = "django.test.emailservice@gmail.com";
        String subject = "Совпадения по объявлению";
        String content = "По вашему обьявлению были найдены такие совпадения:<br>"
                + Urls.toString();

        formEmail(email,fromAddress,subject,content);
    }
}
