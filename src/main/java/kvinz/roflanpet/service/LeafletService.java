package kvinz.roflanpet.service;


import kvinz.roflanpet.domain.Leaflet;
import org.springframework.cglib.core.Predicate;

import java.util.List;

public interface LeafletService {

    List<Leaflet> findAllLeaflets();

    Leaflet fondLeafletsById(Integer id);

    List<Leaflet> findLeafletsAndSortByValueWithFilter(Integer pageNo, Integer pageSize,
                                                       String parameter,String status,Integer cityId,
                                                       String PetGender, String PetSize,Integer PetSpeciesId,
                                                       Integer PetBreadId);
}
