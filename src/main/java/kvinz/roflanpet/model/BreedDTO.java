package kvinz.roflanpet.model;


import kvinz.roflanpet.domain.Species;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BreedDTO {

    private Integer id;

    private String name;

    private Species speciesId;
}
