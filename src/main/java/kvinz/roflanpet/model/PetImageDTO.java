package kvinz.roflanpet.model;

import kvinz.roflanpet.domain.Pet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PetImageDTO {
    private Integer id;

    private String pic;

    private Pet petId;
}
