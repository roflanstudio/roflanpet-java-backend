package kvinz.roflanpet.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import kvinz.roflanpet.domain.Leaflet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MatchingModel {
    @JsonProperty("regular_matching")
    private List<Leaflet> regularMatchingLeaflets;
    @JsonProperty("strong_matching_genders")
    private List<Leaflet> strongGenderMatchingLeaflets;
    @JsonProperty("strong_matching_size")
    private   List<Leaflet> strongSizeMatchingLeaflets;
    @JsonProperty("very_strong_matching")
    private   List<Leaflet> veryStrongMatching;
}
