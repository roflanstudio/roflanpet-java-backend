package kvinz.roflanpet.model;

import kvinz.roflanpet.domain.Breed;
import kvinz.roflanpet.domain.Species;
import kvinz.roflanpet.domain.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PetDTO {

    private Integer id;

    private String name;

    private String gender;

    private String size;

    private String color;

    private String extras;

    private Breed breedId;

    private User finderId;

    private User ownerId;

    private Species speciesId;
}
