package kvinz.roflanpet.model;

import kvinz.roflanpet.domain.City;
import kvinz.roflanpet.domain.Pet;
import kvinz.roflanpet.domain.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LeafletDTO {

    private Integer id;

    private Timestamp dateCreated;

    private City cityId;

    private User creatorId;

    private Integer hitCounterId;

    private Pet petId;

    private Integer qrCounterId;

    private Boolean resolved;
}
