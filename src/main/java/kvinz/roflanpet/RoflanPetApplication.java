package kvinz.roflanpet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RoflanPetApplication {

    public static void main(String[] args) {
        SpringApplication.run(RoflanPetApplication.class, args);
    }

}
