package kvinz.roflanpet.mapper;

import kvinz.roflanpet.domain.PetImage;
import kvinz.roflanpet.model.PetImageDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PetImageMapper {
    PetImageMapper INSTANCE = Mappers.getMapper(PetImageMapper.class);

    PetImage petImageDTOToPetImage(PetImageDTO petImageDTO);
    PetImageDTO petImageToPetImageDTO(PetImage petImage);
}
