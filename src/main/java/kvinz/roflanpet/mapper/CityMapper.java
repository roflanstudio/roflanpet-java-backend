package kvinz.roflanpet.mapper;

import kvinz.roflanpet.domain.City;
import kvinz.roflanpet.model.CityDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CityMapper {
    CityMapper INSTANCE = Mappers.getMapper(CityMapper.class);

    City cityDTOtoCity(CityDTO cityDTO);
    CityDTO cityToCityDTO(City city);
}
