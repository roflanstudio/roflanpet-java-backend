package kvinz.roflanpet.mapper;

import kvinz.roflanpet.domain.Breed;
import kvinz.roflanpet.model.BreedDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface BreedMapper {
    BreedMapper INSTANCE = Mappers.getMapper(BreedMapper.class);

    Breed breedDTOtoBreed(BreedDTO breedDTO);
    BreedDTO breedtoBreedDTO(Breed breedDTO);
}
