package kvinz.roflanpet.mapper;

import kvinz.roflanpet.domain.Leaflet;
import kvinz.roflanpet.model.LeafletDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface LeafletMapper {
    LeafletMapper INSTANCE = Mappers.getMapper(LeafletMapper.class);

    Leaflet leafletDTOToLeaflet(LeafletDTO leafletDTO);
    LeafletDTO leafletToLeafletDto(Leaflet leaflet);
    List<LeafletDTO> leafletListToLeafletDTOList(List<Leaflet> leafletList);
}
