package kvinz.roflanpet.mapper;

import kvinz.roflanpet.domain.Species;
import kvinz.roflanpet.model.SpeciesDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface SpeciesMapper {
    SpeciesMapper INSTANCE = Mappers.getMapper(SpeciesMapper.class);

    Species speciesDTOToSpecies(SpeciesDTO speciesDTO);
    SpeciesDTO speciesToSpeicesDTO(Species species);
}
