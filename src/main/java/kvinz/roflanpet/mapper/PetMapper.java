package kvinz.roflanpet.mapper;

import kvinz.roflanpet.domain.Pet;
import kvinz.roflanpet.model.PetDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PetMapper {
    PetMapper INSTANCE = Mappers.getMapper(PetMapper.class);
    Pet petDTOToPet(PetDTO petDTO);
    PetDTO petToPetDTO(Pet pet);
}
