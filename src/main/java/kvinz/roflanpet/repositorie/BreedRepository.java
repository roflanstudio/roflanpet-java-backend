package kvinz.roflanpet.repositorie;

import kvinz.roflanpet.domain.Breed;
import kvinz.roflanpet.domain.Species;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BreedRepository extends JpaRepository<Breed,Integer> {
    List<Breed> findBySpeciesId_Id(Integer id);
}
