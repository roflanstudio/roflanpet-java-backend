package kvinz.roflanpet.repositorie;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;
import kvinz.roflanpet.domain.Breed;
import kvinz.roflanpet.domain.Leaflet;
import org.apache.tomcat.jni.Address;
import org.springframework.cglib.core.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;

import java.util.List;

public interface LeafletRepository extends JpaRepository<Leaflet,Integer>{

    List<Leaflet> findAllByResolvedFalse(Pageable paging);
    List<Leaflet> findAllByResolvedFalseAndStatus(Pageable paging,String status);
    List<Leaflet> findAllByStatusIsNot(String status);

}
