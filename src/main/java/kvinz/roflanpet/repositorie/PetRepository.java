package kvinz.roflanpet.repositorie;

import kvinz.roflanpet.domain.Pet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetRepository extends JpaRepository<Pet,Integer> {
}
