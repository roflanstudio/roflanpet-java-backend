package kvinz.roflanpet.repositorie;

import kvinz.roflanpet.domain.PetImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetImageRepository extends JpaRepository<PetImage,Integer> {
}
