package kvinz.roflanpet.repositorie;

import kvinz.roflanpet.domain.Breed;
import kvinz.roflanpet.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Integer> {
}
