package kvinz.roflanpet.repositorie;

import kvinz.roflanpet.domain.Breed;
import kvinz.roflanpet.domain.Species;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SpeciesRepository extends JpaRepository<Species,Integer> {
}
