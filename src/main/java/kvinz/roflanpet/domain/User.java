package kvinz.roflanpet.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "accounts_user")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Id
    @Column(name = "id")
    private Integer id;

    @JsonIgnore
    @Column(name = "password")
    private String password;

    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_login", columnDefinition = "timestamp with time zone not null")
    private Date lastLogin;

    @Column(name = "email")
    private String email;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @JsonIgnore
    @Column(name = "is_admin")
    private Boolean isAdmin;

    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "registered", columnDefinition = "timestamp with time zone not null")
    private Date registered;
}
