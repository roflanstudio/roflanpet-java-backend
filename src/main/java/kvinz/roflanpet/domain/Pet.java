package kvinz.roflanpet.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "pets_pet")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pet {
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "gender")
    private String gender;

    @Column(name = "size")
    private String size;

    @Column(name = "color")
    private String color;

    @Column(name = "extras",columnDefinition= "TEXT")
    private String extras;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    @JoinColumn(name="breed_id")
    private Breed breed;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    @JoinColumn(name="finder_id")
    private User finder;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    @JoinColumn(name="owner_id")
    private User owner;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    @JoinColumn(name="species_id")
    private Species species;


    @OneToMany(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL,mappedBy = "petId")
    @JsonManagedReference
    private List<PetImage> petImage;
}
