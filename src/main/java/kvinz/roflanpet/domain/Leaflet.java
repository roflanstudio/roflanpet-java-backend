package kvinz.roflanpet.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "leaflets_leaflet")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Leaflet {
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "date_created")
    private Timestamp dateCreated;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    @JoinColumn(name="city_id")
    private City city;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    @JoinColumn(name="creator_id")
    private User creator;

    @Column(name = "hit_counter_id")
    private Integer hitCounterId;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    @JoinColumn(name="pet_id")
    private Pet pet;

    @Column(name = "qr_counter_id")
    private Integer qrCounterId;

    @Column(name = "resolved")
    private Boolean resolved;

    @Column(name = "status")
    private String status;

    @PrePersist
    public void setDateCreated() {
        this.dateCreated =  new Timestamp(System.currentTimeMillis());
    }

}
