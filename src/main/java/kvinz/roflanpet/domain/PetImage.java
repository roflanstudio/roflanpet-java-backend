package kvinz.roflanpet.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "pets_petimage")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PetImage {
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "pic")
    private String pic;


    @ManyToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL,optional = false)
    @JoinColumn(name="pet_id")
    @JsonBackReference
    private Pet petId;

}
