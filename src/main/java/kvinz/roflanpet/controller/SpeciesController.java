package kvinz.roflanpet.controller;

import kvinz.roflanpet.domain.Species;
import kvinz.roflanpet.service.SpeciesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/species")
public class SpeciesController {

    @Autowired
    private SpeciesService speciesService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Species> getSpecies(){
        return speciesService.findAllSpecies();
    }
}
