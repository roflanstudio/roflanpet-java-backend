package kvinz.roflanpet.controller;


import kvinz.roflanpet.domain.Breed;
import kvinz.roflanpet.service.BreedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/bread" )
public class BreedController {

    @Autowired
    private BreedService breedService;


    @GetMapping("/{speciesId}" )
    @ResponseStatus(HttpStatus.OK)
    public List<Breed> getLeaflets(@PathVariable Integer speciesId){
        return breedService.findBreedBySpeciesId(speciesId);
    }
}
