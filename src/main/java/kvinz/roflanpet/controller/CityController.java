package kvinz.roflanpet.controller;

import kvinz.roflanpet.domain.City;
import kvinz.roflanpet.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/city")
@CrossOrigin
public class CityController {

    @Autowired
    private CityService cityService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<City> getAllCity(){
        return cityService.findAllCity();
    }
}
