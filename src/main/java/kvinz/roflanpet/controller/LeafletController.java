package kvinz.roflanpet.controller;

import kvinz.roflanpet.domain.*;
import kvinz.roflanpet.model.MatchingModel;
import kvinz.roflanpet.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/leaflet")
public class LeafletController {

    @Autowired
    private PetService petService;


    @Autowired
    private LeafletService leafletService;


//    Resolved = false
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Leaflet> getLeaflets(   @RequestParam(defaultValue = "0") Integer pageNo,
                                        @RequestParam(defaultValue = "100") Integer pageSize,
                                        @RequestParam(defaultValue = "id") String sortBy,
                                        @RequestParam(defaultValue = "s",required=false)  String status,
                                        @RequestParam(required=false)  Integer cityId,
                                        @RequestParam(required=false)  String petGender,
                                        @RequestParam(required=false)  String petSize,
                                        @RequestParam(required=false)  Integer petSpeciesId,
                                        @RequestParam(required=false)  Integer petBreadId){
        return leafletService.findLeafletsAndSortByValueWithFilter(pageNo,pageSize,sortBy,status,cityId,petGender
                ,petSize,petSpeciesId,petBreadId);
    }

    @GetMapping("/get/matching")
    @ResponseStatus(HttpStatus.OK)
    public MatchingModel getLeafletsById(@RequestParam Integer leafletId){
        return petService.findMatching(leafletId);
    }


    @GetMapping("/{leafletsId}")
    @ResponseStatus(HttpStatus.OK)
    public Leaflet getLeafletById(@PathVariable Integer leafletsId){
        return leafletService.fondLeafletsById(leafletsId);
    }









}
